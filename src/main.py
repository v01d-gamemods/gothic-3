from trainerbase.main import run

from gui import run_menu
from injections import update_last_used_item_count_pointer
from scripts import script_engine


def on_initialized():
    update_last_used_item_count_pointer.inject()
    script_engine.start()


def on_shutdown():
    script_engine.stop()


if __name__ == "__main__":
    run(run_menu, on_initialized, on_shutdown)
