from trainerbase.gameobject import GameUnsignedInt
from trainerbase.scriptengine import AbstractBaseScript, ScriptEngine

from objects import xp


script_engine = ScriptEngine()


class XPMultiplier(AbstractBaseScript):
    def __init__(self, xp: GameUnsignedInt):
        self.last_check_xp = -1
        self.xp = xp
        self.factor = 1

    def __call__(self):
        if 0 < self.last_check_xp < xp.value:
            xp.value += (xp.value - self.last_check_xp) * (self.factor - 1)

        self.last_check_xp = xp.value


multiply_xp = script_engine.register_script(XPMultiplier(xp))
